<?php

namespace App\Entity;

use App\Repository\ReportRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 */
class Report
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbWastes;

    /**
     * @ORM\Column(type="integer")
     */
    private $sortedWastes;

    /**
     * @ORM\Column(type="float")
     */
    private $totalOfCo2Emitted;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNbWastes(): ?int
    {
        return $this->nbWastes;
    }

    public function setNbWastes(int $nbWastes): self
    {
        $this->nbWastes = $nbWastes;

        return $this;
    }

    public function getSortedWastes(): ?int
    {
        return $this->sortedWastes;
    }

    public function setSortedWastes(int $sortedWastes): self
    {
        $this->sortedWastes = $sortedWastes;

        return $this;
    }

    public function getTotalOfCo2Emitted(): ?float
    {
        return $this->totalOfCo2Emitted;
    }

    public function setTotalOfCo2Emitted(float $totalOfCo2Emitted): self
    {
        $this->totalOfCo2Emitted = $totalOfCo2Emitted;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
