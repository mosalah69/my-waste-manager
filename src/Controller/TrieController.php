<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TrieController extends AbstractController
{
    /**
     * @Route("/trie", name="trie", methods="GET")
     */ public function AllPopulation()
    {
        $json = file_get_contents('../data.json');
        $parsed_json = json_decode($json);
        for ($index=0; $index < 12 ; $index++)
        { 
            $this->population = $parsed_json->{'quartiers'}[$index]->{'population'};
        }
        return $this->json('tout est ok', Response::HTTP_CREATED);
    }
    // public function index()
    // {
    //     return $this->json([
    //         'message' => 'Welcome to your new controller!',
    //         'path' => 'src/Controller/TrieController.php',
    //     ]);
    // }


}
